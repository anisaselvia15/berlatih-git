<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@utama');
Route::get('/table', 'AuthController@daftar');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('halaman.datatable');
});

//CRUD Cast

//Create data cast

//Masuk ke form cast
Route::get('/cast/create', 'CastController@create');
//untuk kirim inputan ke table cast
Route::post('/cast', 'CastController@store');

//Read Data Cast

//Tampil semua data cast
Route::get('/cast', 'CastController@index');
//detail cast berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

//Update Data Cast

//masuk ke form cast berdasarkan id
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk update data inputan berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete Data Cast

//Delete data berdasarkan id
Route::delete('/cast/{cast_id}', 'CastController@destroy');