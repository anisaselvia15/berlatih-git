<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:6',            
        ],
        [
            'nama.required' => 'Nama Cast tidak boleh kosong harus diisi!',
            'umur.required' => 'Umur Cast tidak boleh kosong harus diisi!',
            'bio.required' => 'Bio Cast tidak boleh kosong harus diisi!',  
            'bio.min' => 'Bio min 6 karakter!',  
        ]
           
        );

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');

    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        //dd($cast);
 
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id)
    {
        $cast = DB::table('cast')->where('cast_id', $cast_id)->first();

        return view('cast.detail', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')->where('cast_id', $cast_id)->first();

        return view('cast.update', compact('cast'));
    }

    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:6',            
        ],
        [
            'nama.required' => 'Nama Cast tidak boleh kosong harus diisi!',
            'umur.required' => 'Umur Cast tidak boleh kosong harus diisi!',
            'bio.required' => 'Bio Cast tidak boleh kosong harus diisi!',  
            'bio.min' => 'Bio min 6 karakter!',  
        ]
           
        );

        DB::table('cast')
              ->where('cast_id', $cast_id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]
            );

        
        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        DB::table('cast')->where('cast_id', $cast_id)->delete();

        return redirect('/cast');
    }
}

